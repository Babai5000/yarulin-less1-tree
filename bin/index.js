#!/usr/bin/env node

const {argv} = require('node:process');
const process = require('node:process');
const path = require('node:path');
const fs = require('node:fs');
const { Command, InvalidArgumentError } = require('commander')
const TreeView = require('yarulin-less1-tree');


function myParseInt(value) {
    if (value === null) {
        return value
    }
    const parsedValue = parseInt(value, 10);
    if (isNaN(parsedValue)) {
        throw new InvalidArgumentError('Not a number')
    }
    if (parsedValue < 0) {
        throw new InvalidArgumentError('Number less than zero')
    }
    return parsedValue;
}

try {
    const program = new Command()
    program
        .version('1.0.0')
        .description('Show dir as tree')
        .argument('<dirpath>', 'Directory path')
        .option('-d, --depth <depth>', 'separator character', myParseInt, null)
        .action((dirpath, option) => {
            try {
                
                const processPath = process.cwd()
                const curPath = path.join(processPath, dirpath)
                
                const createStructForTree = (curPath) => {
                    const dirName = path.basename(curPath)
                    const partTree = {
                        name: dirName,
                        items: []
                    }

                    const dirItems = fs.readdirSync(curPath, 'utf8')
                    for (let itemName of dirItems) {
                        const itemPath = path.join(curPath,`/${itemName}`)
                        const statsDir = fs.statSync(itemPath)
                        if (statsDir.isDirectory()) {
                            const dirItem = createStructForTree(itemPath)
                            partTree.items.push(dirItem)
                            continue
                        }

                        partTree.items.push({ name: itemName})
                    }
                    return partTree
                }
                const dirStruct = createStructForTree(curPath)
                const treeView = TreeView.CreatTree(dirStruct, 0, option.depth)
                console.log(treeView.getTreeViewList().join("\n"))
                const countDirs = treeView.getNodeCount()
                const countFiles = treeView.getLeafCount()
                console.log(`${countDirs} directories, ${countFiles} files`)
                
            } catch (e) {
                console.log(e)
            }

        })
    program.parse(argv)
} catch (e){
    console.log(e.message)
}


