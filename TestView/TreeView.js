const TREE_PART = {
    MAIN_ROOT_PART: "│",
    ITEM: "├──",
    LAST_ITEM: "└──",
    SPASE: " "
}

const Leaf = class Leaf {
    name = ""
    depth = 1
    isLast = false
    constructor(name = "", depth = 1) {
        this.name = name
        this.depth = depth
    }

    setIsLastItem(isLast = false) {
        this.isLast = isLast
    }

    getView() {
        const isFirstDepth = this.depth === 1
        const viewSpace = isFirstDepth
            ? ``
            : new Array(this.depth === 0 ? this.depth : this.depth - 1)
                .fill(TREE_PART.SPASE)
                .join(``)
        const viewItem = this.isLast
            ? `${TREE_PART.LAST_ITEM}${this.name}`
            : `${TREE_PART.ITEM}${this.name}`
        const viewMain = isFirstDepth
            ? ``
            : TREE_PART.MAIN_ROOT_PART

        return `${viewMain}${viewSpace}${viewItem}`
    }
}
const Node = class Node extends Leaf {
    items = []
    constructor(name = "", depth = 0, items = []) {
        super(name, depth)
        this.items = items
    }

    addItem(item) {
        if (item instanceof Leaf || item instanceof Node) {
            this.items.push(item)
        }
    }

    isRootNode() {
        return this.depth === 0
    }

    getView() {
        let view = this.isRootNode() ? this.name : super.getView()
        view = Array.isArray(view) ? view : [ view ]
        
        const viewChilds = this.items.length ? this.items.map( i => i.getView()).flat() : []
        
        return [ ...view, ...viewChilds ] 
    }
}

const createItemTree = (name, depth, hasItems = false) => {
    return hasItems 
        ? new Node(name, depth, [])
        : new Leaf(name, depth)
}

const isNodeStruct = (struct) => struct.hasOwnProperty(`items`) && Array.isArray(struct.items)

const buildTree = (struct, depth = 0, endDepth = null) => {

    if (depth === 0 && !Array.isArray(struct.items)) {
        throw new Error(`Incorrect struct data`)
    }

    if (!struct.name) {
        throw new Error(`Incorrect struct data, name is empty or undefined`)
    }

    const structName = struct.name

    if (isNodeStruct(struct)) {
        const node = createItemTree(structName, depth, true)
        const itemLength = struct.items.length
        if (
            struct.items.length > 0 && endDepth === null
            || struct.items.length > 0 && endDepth !== null && depth < endDepth
        ) {
            struct.items.forEach((item, key) => {
                const treeItem = buildTree(item, depth + 1, endDepth)
                if (key === itemLength - 1) {
                    treeItem.setIsLastItem(true)
                }
                node.addItem(treeItem)
            })
        }
        return node
    }

    return createItemTree(structName, depth)
}

const TreeView = class TreeView {
    constructor(treeStruct) {

        if (!(treeStruct instanceof Node) || treeStruct.depth !== 0) {
            throw new Error(`Incorrect tree data`)
        }

        this.tree = treeStruct
    }

    static CreatTree(struct, depth = 0, endDepth = null) {
        return new TreeView(buildTree(struct, depth, endDepth))
    }

    getTreeByDepth(endDepth = 0) {
        if (typeof endDepth !== "number" || isNaN(endDepth)) {
            throw new Error(`Incorrect depth value`)
        }
        
        if (endDepth < 0) {
            throw new Error(`Depth must be more or equal zero`)
        }

        return TreeView.CreatTree(buildTree(this.tree, 0, endDepth))
    }

    getTreeViewList() {
        return this.tree.getView()
    }

    getNodeCount() {
        const fnGetNode = (partTree, countNode) => {
            if (partTree instanceof Node) {
                if (!partTree.isRootNode()) {
                    countNode++
                }
                for (let item of partTree.items) {
                    countNode = fnGetNode(item, countNode)
                }
            }
            return countNode
        }
        return fnGetNode(this.tree, 0)
    }

    getLeafCount() {
        const fnGetLeaf = (tree, countLeaf) => {
            return tree.reduce((acc, partTree) => {
                if (partTree instanceof Leaf && !(partTree instanceof Node)) {
                    acc++
                }
                if (partTree instanceof Node) {
                    for (let item of partTree.items) {
                        acc = fnGetLeaf([item], acc)
                    }
                }
                return acc
            }, countLeaf)
        }
        return fnGetLeaf([this.tree], 0)
    }
}

module.exports = {
    TreeView,
    Leaf,
    Node,
    createItemTree,
    buildTree,
}
