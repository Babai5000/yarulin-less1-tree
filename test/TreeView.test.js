const {TreeView, Leaf, Node, createItemTree, buildTree} = require("../TestView/TreeView")
const {stringify} = require('flatted')


describe(`Test TreeView`, () => {

    describe(`Fail create TreeView`, () => {
        test('Passing the string as data', () => {
            const dataText = `some text`
            expect(() => TreeView.CreatTree(dataText)).toThrow(/^Incorrect struct data$/);
        })
    
        test('Passing the number as data', () => {
            const dataNumber = 1
            expect(() => TreeView.CreatTree(dataNumber)).toThrow(/^Incorrect struct data$/);
        })
    
        test('Passing the function as data', () => {
            const dataFn = () => {}
            expect(() => TreeView.CreatTree(dataFn)).toThrow(/^Incorrect struct data$/);
        })
    
        test('Passing the boolean as data', () => {
            const dataBoolean = false
            expect(() => TreeView.CreatTree(dataBoolean)).toThrow(/^Incorrect struct data$/);
        })
    
        test('Passing the array as data', () => {
            const dataArray = []
            expect(() => TreeView.CreatTree(dataArray)).toThrow(/^Incorrect struct data$/);
        })
    
        test('Passing empty object as data', () => {
            const dataObject = {}
            expect(() => TreeView.CreatTree(dataObject)).toThrow(/^Incorrect struct data$/);
        })

        test('Passing incorrect object as data', () => {
            const dataObject = {test: `test`}
            expect(() => TreeView.CreatTree(dataObject)).toThrow(/^Incorrect struct data$/);
        })

        test('Passing incorrect object as data, name not defined', () => {
            const dataObject = {items: []}
            expect(() => TreeView.CreatTree(dataObject)).toThrow(/^Incorrect struct data, name is empty or undefined$/);
        })

        test('Passing incorrect object as data', () => {
            const dataObject = {name: `test`}
            expect(() => new TreeView(new Leaf(dataObject, 0))).toThrow(/^Incorrect tree data$/);
        })

    })

    describe(`Create Node and Leaf`, () => {

        describe(`Test Node`, () => {
            const node = createItemTree(`test`, 2, true)

            test('Create Node', () => {
                expect(node).toBeInstanceOf(Node);

                expect(node.depth).toBe(2);
                expect(node.name).toBe(`test`);
            })

            test('Node is last', () => {
                expect(node.isLast).toBeFalsy()
                node.setIsLastItem(true)
                expect(node.isLast).not.toBeFalsy()
            })

            test('Node is root', () => {
                const nodeRoot = createItemTree(`test`, 0, true)
                expect(nodeRoot.isRootNode()).not.toBeFalsy()

                const nodeNotRoot = createItemTree(`test`, 1, true)
                expect(nodeNotRoot.isRootNode()).toBeFalsy()
            })

            test('Node view', () => {
                const nodeRoot = createItemTree(`test`, 0, true)
                expect(nodeRoot.getView()).toEqual([`test`])

                const nodeNotRootName = `nodeNotRoot`
                const nodeNotRoot = createItemTree(nodeNotRootName, 1, true)
                expect(nodeNotRoot.getView()).toEqual([`├──${nodeNotRootName}`])
                nodeNotRoot.setIsLastItem(`true`)
                expect(nodeNotRoot.getView()).toEqual([`└──${nodeNotRootName}`])


                const nodeDepth3Name = `nodeDepth3`
                const nodeDepth3 = createItemTree(nodeDepth3Name, 3, true)
                expect(nodeDepth3.getView()).toEqual([`│  ├──${nodeDepth3Name}`])
                nodeDepth3.setIsLastItem(`true`)
                expect(nodeDepth3.getView()).toEqual([`│  └──${nodeDepth3Name}`])
            })

            test('Node items length(addItems)', () => {
                const nodeRoot = createItemTree(`test`, 0, true)
                const node1 = createItemTree(`test1`, 1, true)
                const node2 = createItemTree(`test2`, 1, true)
                const leaf = createItemTree(`test leaf`, 1)
                const obj1 = {dasdad: `dsfsd`}
                const obj2 = new Error(`Test`)

                expect(nodeRoot.items).toHaveLength(0)

                nodeRoot.addItem(node1)
                nodeRoot.addItem(node2)
                expect(nodeRoot.items).toHaveLength(2)

                nodeRoot.addItem(obj1)
                nodeRoot.addItem(obj2)
                expect(nodeRoot.items).toHaveLength(2)

                nodeRoot.addItem(leaf)
                expect(nodeRoot.items).toHaveLength(3)
                
            })
        })
        

        describe(`Test Leaf`, () => {
            const leaf = createItemTree(`test`, 2)

            test('Create Leaf from function', () => {
                
                expect(leaf).not.toBeInstanceOf(Node);
                expect(leaf).toBeInstanceOf(Leaf);

                expect(leaf.depth).toBe(2);
                expect(leaf.name).toBe(`test`);

            })

            test('Create Leaf from constructor', () => {
                const a = Symbol()
                const b = Symbol()
                const leaf = new Leaf(a, b)
                expect(leaf).toBeInstanceOf(Leaf)
                expect(leaf.depth).toBe(b)
                expect(leaf.name).toBe(a)

            })

            test('Leaf is last', () => {
                expect(leaf.isLast).toBeFalsy()
                leaf.setIsLastItem(true)
                expect(leaf.isLast).not.toBeFalsy()
            })

            test('Leaf view', () => {

                const leafName = `nodeNotRoot`
                const leafNode = createItemTree(leafName, 1)
                expect(leafNode.getView()).toEqual(`├──${leafName}`)
                leafNode.setIsLastItem(`true`)
                expect(leafNode.getView()).toEqual(`└──${leafName}`)


                const leafNode3Name = `nodeDepth3`
                const leafNode3 = createItemTree(leafNode3Name, 3)
                expect(leafNode3.getView()).toEqual(`│  ├──${leafNode3Name}`)
                leafNode3.setIsLastItem(`true`)
                expect(leafNode3.getView()).toEqual(`│  └──${leafNode3Name}`)
            })

        })
    })
    
    describe(`Create TreeView`, () => {

        test('Passing correct data', () => {

            const toStringTreeView = jest.fn(struct => {
                const tree = TreeView.CreatTree(struct)
                return stringify(tree)
            })
            
            const data1 = {name: `Main node`, items: []}
            const data2 = {
                "name": 1,
                "items": [{
                    "name": 2,
                    "items": [{ "name": 3 }, { "name": 4 }]
                }, {
                    "name": 5,
                    "items": [{ "name": 6 }]
                }]
            }
            
            const data1Res = `[{"tree":"1"},{"name":"2","depth":0,"isLast":false,"items":"3"},"Main node",[]]`
            const data2Res = `[{"tree":"1"},{"name":1,"depth":0,"isLast":false,"items":"2"},["3","4"],{"name":2,"depth":1,"isLast":false,"items":"5"},{"name":5,"depth":1,"isLast":true,"items":"6"},["7","8"],["9"],{"name":3,"depth":2,"isLast":false},{"name":4,"depth":2,"isLast":true},{"name":6,"depth":2,"isLast":true}]`
    
            toStringTreeView(data1)
            toStringTreeView(data2)

            expect(toStringTreeView).toHaveNthReturnedWith(1, data1Res)
            expect(toStringTreeView).toHaveNthReturnedWith(2, data2Res)
        })
        
    })

    describe(`Nodes has correct childs`, () => {
        const data = {
            "name": 1,
            "items": [
                {
                    "name": 2,
                    "items": [{ "name": 3 }, { "name": 4 }]
                }, 
                {
                    "name": 5,
                    "items": [{ "name": 6 }]
                }, 
                { "name": 7 }
            ]
        }

        const expectedChildNames = [`Node`, `Node`, `Leaf`]

        const tree = TreeView.CreatTree(data)
        
        test('Nodes has 3 childs', () => {
            expect(tree.tree.items).toHaveLength(3)
        })

        const itemsTestingNames = tree.tree.items.map((child, index) => ({ child, expected: expectedChildNames[index] }))
        describe.each(itemsTestingNames)(`Test child class names`, ({ child, expected }) => {
            test(`Child class name is ${expected}`, () => {
                expect(child.constructor.name).toEqual(expected)
            })
        })

    })

    describe(`Clone tree with the passed depth`, () => {

        const toStringCloneTree = jest.fn((struct, depth) => {
            const tree = TreeView.CreatTree(struct)
            return stringify(tree.getTreeByDepth(depth))
        })

        const data = {
            "name": 1,
            "items": [{
                "name": 2,
                "items": [{ "name": 3 }, { "name": 4 }]
            }, {
                "name": 5,
                "items": [{ "name": 6 }]
            }]
        }

        describe(`Fail clone tree`, () => {

            const tree = TreeView.CreatTree(data)

            test('Passing incorrect depth', () => {
                expect(() => tree.getTreeByDepth({})).toThrow(/^Incorrect depth value$/);
                expect(() => tree.getTreeByDepth(NaN)).toThrow(/^Incorrect depth value$/);
                expect(() => tree.getTreeByDepth([])).toThrow(/^Incorrect depth value$/);
                expect(() => tree.getTreeByDepth(``)).toThrow(/^Incorrect depth value$/);
            })

            test('Depth less than zero', () => {
                expect(() => tree.getTreeByDepth(-10)).toThrow(/^Depth must be more or equal zero$/);
            })

        })

        describe(`Correct depth`, () => {

            test('Depth value not passed, default depth must be 0', () => {
                const tree = TreeView.CreatTree(data)
                const dataRes = `[{"tree":"1"},{"name":1,"depth":0,"isLast":false,"items":"2"},[]]`
           
                expect(stringify(tree.getTreeByDepth())).toEqual( dataRes)
            })

            test('Passing correct depth 1', () => {

                const dataRes = `[{\"tree\":\"1\"},{\"name\":1,\"depth\":0,\"isLast\":false,\"items\":\"2\"},[\"3\",\"4\"],{\"name\":2,\"depth\":1,\"isLast\":false,\"items\":\"5\"},{\"name\":5,\"depth\":1,\"isLast\":true,\"items\":\"6\"},[],[]]`
    
                toStringCloneTree(data, 1)

                expect(toStringCloneTree).toHaveReturnedWith( dataRes)
    
            })
    
            test('Passing depth more depth in tree', () => {
    
                const dataRes = `[{\"tree\":\"1\"},{\"name\":1,\"depth\":0,\"isLast\":false,\"items\":\"2\"},[\"3\",\"4\"],{\"name\":2,\"depth\":1,\"isLast\":false,\"items\":\"5\"},{\"name\":5,\"depth\":1,\"isLast\":true,\"items\":\"6\"},[\"7\",\"8\"],[\"9\"],{\"name\":3,\"depth\":2,\"isLast\":false},{\"name\":4,\"depth\":2,\"isLast\":true},{\"name\":6,\"depth\":2,\"isLast\":true}]`
    
                toStringCloneTree(data, 100)
    
                expect(toStringCloneTree).toHaveReturnedWith(dataRes)
    
            })
        })
        

        
    })

    //getNodeCount
    describe(`Get nodes in tree`, () => {
        const data = {
            "name": 1,
            "items": [{
                "name": 2,
                "items": [{ "name": 3 }, { "name": 4 }]
            }, {
                "name": 5,
                "items": [
                    { 
                        "name": 7,
                        "items": [
                            { 
                                "name": 8,
                                "items": [
                                    { "name": 9 }
                                ]
                            }
                        ]
                    }
                ]
            }]
        }

        test('Get nodes in tree', () => {

            const tree = TreeView.CreatTree(data)
            const treeMaxDepsOne = tree.getTreeByDepth(1)
            const treeMaxDepsZero = tree.getTreeByDepth(0)
            
            expect(tree.getNodeCount()).toEqual(4)
            expect(treeMaxDepsOne.getNodeCount()).toEqual(2)
            expect(treeMaxDepsZero.getNodeCount()).toEqual(0)

        })

    })

    //getLeafCount
    describe(`Get leafs in tree`, () => {
        const data = {
            "name": 1,
            "items": [{
                "name": 2,
                "items": [{ "name": 3 }, { "name": 4 }]
            }, {
                "name": 5,
                "items": [
                    { 
                        "name": 7,
                        "items": [
                            { 
                                "name": 8,
                                "items": [
                                    { "name": 9 }
                                ]
                            }
                        ]
                    },
                    { "name": 10 }
                ]
            },
            { "name": 11 }]
        }

        test('Get leafs in tree', () => {

            const tree = TreeView.CreatTree(data)
            const treeMaxDepsOne = tree.getTreeByDepth(1)
            const treeMaxDepsZero = tree.getTreeByDepth(0)
            
            expect(tree.getLeafCount()).toEqual(5)
            expect(treeMaxDepsOne.getLeafCount()).toEqual(1)
            expect(treeMaxDepsZero.getLeafCount()).toEqual(0)

        })
    })

    //getLeafCount
    describe(`Get TreeView List`, () => {

        const data = {
            "name": 1,
            "items": [{
                "name": 2,
                "items": [{ "name": 3 }, { "name": 4 }]
            }, {
                "name": 5,
                "items": [
                    { 
                        "name": 7,
                        "items": [
                            { 
                                "name": 8,
                                "items": [
                                    { "name": 9 }
                                ]
                            }
                        ]
                    },
                    { "name": 10 }
                ]
            },
            { "name": 11 }]
        }

        test('Get TreeView List', () => {

            const tree = TreeView.CreatTree(data)
            const treeMaxDepsOne = tree.getTreeByDepth(1)
            const treeMaxDepsZero = tree.getTreeByDepth(0)

            const listViewTree = [ 1, '├──2', '│ ├──3', '│ └──4', '├──5', '│ ├──7', '│  └──8', '│   └──9', '│ └──10', '└──11']
            const listViewTreeMaxDepsOne = [ 1, '├──2', '├──5', '└──11' ]
            const listViewTreeMaxDepsZero = [ 1 ]
            
            expect(tree.getTreeViewList()).toEqual(listViewTree)
            expect(treeMaxDepsOne.getTreeViewList()).toEqual(listViewTreeMaxDepsOne)
            expect(treeMaxDepsZero.getTreeViewList()).toEqual(listViewTreeMaxDepsZero)

        })

    })


})


