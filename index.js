const TreeView = require("yarulin-less1-tree")

const data = {
    "name": 1,
    "items": [{
        "name": 2,
        "items": [{ "name": 3 }, { "name": 4 }]
    }, {
        "name": 5,
        "items": [{ "name": 6 }]
    }]
}

const tree = TreeView.CreatTree(data)

console.log(tree.getTreeViewList().join("\n"))
